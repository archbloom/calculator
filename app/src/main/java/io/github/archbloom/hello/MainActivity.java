package io.github.archbloom.hello;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    ArrayList<Button> b;
    TextView textView,textview1;
    Calculator c;
    int prev_button_pressed=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeUI();

        c= new Calculator();
    }

    void initializeUI()
    {
        textView = (TextView)findViewById(R.id.display);
        textview1 = (TextView)findViewById(R.id.display1);

        b=new ArrayList<Button>();

        b.add((Button) findViewById(R.id.one));
        b.add((Button) findViewById(R.id.two));
        b.add((Button) findViewById(R.id.three));
        b.add((Button) findViewById(R.id.plus));
        b.add((Button) findViewById(R.id.four));
        b.add((Button) findViewById(R.id.five));
        b.add((Button) findViewById(R.id.six));
        b.add((Button) findViewById(R.id.minus));
        b.add((Button) findViewById(R.id.seven));
        b.add((Button) findViewById(R.id.eight));
        b.add((Button) findViewById(R.id.nine));
        b.add((Button) findViewById(R.id.mul));
        b.add((Button) findViewById(R.id.zero));
        b.add((Button) findViewById(R.id.dot));
        b.add((Button) findViewById(R.id.equal));
        b.add((Button) findViewById(R.id.divide));
        b.add((Button) findViewById(R.id.sin));
        b.add((Button) findViewById(R.id.cos));
        b.add((Button) findViewById(R.id.tan));
        b.add((Button) findViewById(R.id.mem));

        for(int i=0;i<b.size();i++)
        {
            b.get(i).setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.equal:
                prev_button_pressed = R.id.equal;
                if(validateExpression(textView.getText().toString())) {
                    //calculate
                    String exp = textView.getText().toString();
                    String result = c.Evaluate(exp);
                    textView.setText(result);
                    storeResults(exp,result);
                }
                else
                {
                    Toast.makeText(this,"Invalid expression",Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.mem:
                textview1.setText(readFromfile());
                break;

            default:
                Button b = (Button) findViewById(view.getId());
                textView.setText(setstring(textView.getText().toString(),b.getText().toString()));
                break;
        }
    }

    boolean validateExpression(String exp)
    {
        if(exp.charAt(exp.length()-1)=='+' || exp.charAt(exp.length()-1)=='-' || exp.charAt(exp.length()-1)=='*' || exp.charAt(exp.length()-1)=='/')
            return false;
        else
            return true;
    }

    String readFromfile()
    {
        String s = "";
        try{
            FileInputStream fin = openFileInput("myfile");
            int c;

            while( (c = fin.read()) != -1){
                s = s + Character.toString((char)c);
            }

            Toast.makeText(getBaseContext(),"file read",Toast.LENGTH_SHORT).show();
        }
        catch(Exception e){
        }

        Log.d("log", s);

        return s;
    }

    void storeResults(String exp,String result)
    {
        Log.d("expression",exp+"="+result);
        String filename = "myfile";
        String string = exp+"="+result+"\n";
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(filename, MODE_APPEND);
            outputStream.write(string.getBytes());

            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    String setstring(String a,String b)
    {
        Log.d("a",a);
        Log.d("b",b);
        if(b.equals("."))
        {
            boolean set = true;

            for(int i=0;i<a.length();i++)
            {
                if(a.charAt(i)=='.')
                    set = false;
                else if(a.charAt(i)=='+' || a.charAt(i)=='-' || a.charAt(i)=='*' || a.charAt(i)=='/')
                    set = true;
            }
            return set?a+b:a;
        }
        if(b.equals("+") || b.equals("-") || b.equals("*") || b.equals("/"))
        {
            if(a.charAt(a.length()-1)=='+' || a.charAt(a.length()-1)=='-' || a.charAt(a.length()-1)=='*' || a.charAt(a.length()-1)=='/' )
//            if(a.contains("+") || a.contains("-") || a.contains("*") || a.contains("/"))
                return a;
            else
            {
                String exp = a;
                String result = c.Evaluate(a);
                storeResults(exp,result);
                return result + b;
            }
        }
        if(b.equals("Sin") || b.equals("Cos") || b.equals("Tan"))
        {
            Log.d("sd","sd");
            return b;
        }

        return a+b;

    }
}
