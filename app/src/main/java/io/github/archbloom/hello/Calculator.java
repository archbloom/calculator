package io.github.archbloom.hello;

/**
 * Created by archbloom on 12/4/16.
 */
import android.content.Context;
import android.util.Log;

import java.io.FileOutputStream;

public class Calculator
{
    double num1,num2;


    String Evaluate(String exp)
    {
        String result = "";
        if(exp.contains("+"))
        {
            num1 = Double.parseDouble(exp.substring(0,exp.indexOf("+")));
            num2 = Double.parseDouble(exp.substring(exp.indexOf("+")+1));
            Log.d("num1",num1+"");
            Log.d("num2",num2+"");
            result = ""+add();
        }
        else if(exp.contains("-"))
        {
            num1 = Double.parseDouble(exp.substring(0,exp.indexOf("-")));
            num2 = Double.parseDouble(exp.substring(exp.indexOf("-")+1));
            Log.d("num1",num1+"");
            Log.d("num2",num2+"");
            result = ""+subtract();
        }
        else if(exp.contains("*"))
        {
            num1 = Double.parseDouble(exp.substring(0,exp.indexOf("*")));
            num2 = Double.parseDouble(exp.substring(exp.indexOf("*")+1));
            Log.d("num1",num1+"");
            Log.d("num2",num2+"");
            result =""+multiply();
        }
        else if(exp.contains("/"))
        {
            num1 = Double.parseDouble(exp.substring(0,exp.indexOf("/")));
            num2 = Double.parseDouble(exp.substring(exp.indexOf("/")+1));
            Log.d("num1",num1+"");
            Log.d("num2",num2+"");
            result =""+divide();
        }
        else if(exp.contains("Sin"))
        {
            num1 = Double.parseDouble(exp.substring(3));
            Log.d("num1",num1+"");
            result =""+sine();
        }
        else if(exp.contains("Cos"))
        {
            num1 = Double.parseDouble(exp.substring(3));
            Log.d("num1",num1+"");
            result =""+cos();
        }
        else if(exp.contains("Tan"))
        {
            num1 = Double.parseDouble(exp.substring(3));
            Log.d("num1",num1+"");
            result =""+tan();
        }
        else
        {
            result = exp; //if we press only number and equal button the it should return that number only
        }

        return result;
    }

    double add()
    {
        return (num1+num2);
    }

    double subtract()
    {
        return num1-num2;
    }

    double multiply()
    {
        return num1*num2;
    }

    double divide()
    {
        return num1/num2;
    }

    double sine()
    {
        return (Math.sin(Math.toRadians(num1)));
    }
    double cos()
    {
        return (Math.cos(Math.toRadians(num1)));
    }
    double tan()
    {
        return (Math.tan(Math.toRadians(num1)));
    }



}

